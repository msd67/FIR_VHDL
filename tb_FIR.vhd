LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use ieee.numeric_std.all;
 
ENTITY tb_FIR IS
END tb_FIR;
 
ARCHITECTURE behavior OF tb_FIR IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT FIR
    PORT(
         clk	:	IN 	std_logic;
			Busy	:	OUT 	std_logic;
			Valid	:	IN 	std_logic;
			RDY 	:	OUT 	std_logic;
         Din 	:	IN  	std_logic;
         Dout 	:	OUT  	std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal Din : std_logic := '0';
	signal Valid	:	std_logic	:=	'0';

 	--Outputs
   signal Dout : std_logic;
	signal Busy	: std_logic;
	signal RDY	:	std_logic;
	
	Type	state_type	is	(zero, one, two, three, four, Data_ch);
	signal	state_reg	:	state_type	:=	Data_ch;
	
	signal	counter		:	UNSIGNED( 3 downto 0)	:=	(OTHERS	=>	'0');
	signal	D_count		:	UNSIGNED( 2 downto 0)	:=	(OTHERS	=>	'0');
	
	signal	Result		:	UNSIGNED(13 downto 0)	:=	(OTHERS	=>	'0');
	
	signal	MyData0		:	UNSIGNED(13 downto 0)	:=	(OTHERS	=>	'0');
	signal	MyData1		:	UNSIGNED(13 downto 0)	:=	(OTHERS	=>	'0');
	signal	MyData2		:	UNSIGNED(13 downto 0)	:=	(OTHERS	=>	'0');
	signal	MyData3		:	UNSIGNED(13 downto 0)	:=	(OTHERS	=>	'0');
	signal	MyData4		:	UNSIGNED(13 downto 0)	:=	(OTHERS	=>	'0');

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: FIR PORT MAP (
          clk => clk,
          Din => Din,
			 Valid => Valid,
			 RDY	=>	RDY,
			 Busy => Busy,
          Dout => Dout
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process (clk)
   begin		
      -- hold reset state for 100 ns.
--      wait for 100 ns;	

--      wait for clk_period*10;

      -- insert stimulus here 
		if (rising_edge(clk))	then
		
			if (state_reg = zero and Busy='0')	then
				counter		<=		counter + 1;
				Din			<=		MyData0(13 - to_integer(counter));
				if (counter = to_unsigned(13, 4))	then
					counter		<=		(OTHERS	=>	'0');
					state_reg	<=		one;
				end if;
			end if;
			
			if (state_reg = one and Busy='0')		then
				counter		<=		counter + 1;
				Din			<=		MyData1(13 - to_integer(counter));
				if (counter = to_unsigned(13, 4))	then
					counter		<=		(OTHERS	=>	'0');
					state_reg	<=		two;
				end if;			
			end if;
			
			if (state_reg = two and Busy='0')		then
				counter		<=		counter + 1;
				Din			<=		MyData2(13 - to_integer(counter));
				if (counter = to_unsigned(13, 4))	then
					counter		<=		(OTHERS	=>	'0');
					state_reg	<=		three;
				end if;
			end if;
			
			if (state_reg = three and Busy='0')	then
				counter		<=		counter + 1;
				Din			<=		MyData3(13 - to_integer(counter));
				if (counter = to_unsigned(13, 4))	then
					counter		<=		(OTHERS	=>	'0');
					state_reg	<=		four;
				end if;
			end if;
			
			if (state_reg = four and Busy='0')	then
				counter		<=		counter + 1;
				Din			<=		MyData4(13 - to_integer(counter));
				if (counter = to_unsigned(13, 4))	then
					counter		<=		(OTHERS	=>	'0');
					state_reg	<=		Data_ch;
					Valid			<=		'0';
					D_count		<=		D_count + 1;
				end if;
			end if;
			
			if (state_reg = Data_ch)	then
				Valid			<=		'1';
				state_reg	<=		zero;
				
				if (D_count = to_unsigned(0,3))	then
					MyData0	<=		to_unsigned(623,14);
					MyData1	<=		to_unsigned(956,14);
					MyData2	<=		to_unsigned(1583,14);
					MyData3	<=		to_unsigned(2671,14);
					MyData4	<=		to_unsigned(1401,14);
				end if;
				
				if (D_count = to_unsigned(1,3))	then					
					MyData0	<=		to_unsigned(1623,14);
					MyData1	<=		to_unsigned(1956,14);
					MyData2	<=		to_unsigned(2583,14);
					MyData3	<=		to_unsigned(1671,14);
					MyData4	<=		to_unsigned(1401,14);
				end if;
				
				if (D_count = to_unsigned(2,3))	then					
					MyData0	<=		to_unsigned(4023,14);		-- Big number with 14-bit is 16383
					MyData1	<=		to_unsigned(3556,14);
					MyData2	<=		to_unsigned(2083,14);
					MyData3	<=		to_unsigned(2971,14);
					MyData4	<=		to_unsigned(3201,14);					
				end if;
				
				if (D_count = to_unsigned(3,3))	then					
					MyData0	<=		to_unsigned(14023,14);		-- Big number with 14-bit is 16383
					MyData1	<=		to_unsigned(13556,14);
					MyData2	<=		to_unsigned(12083,14);
					MyData3	<=		to_unsigned(12971,14);
					MyData4	<=		to_unsigned(13201,14);					
				end if;
				
				if (D_count = to_unsigned(4,3))	then					
					MyData0	<=		to_unsigned(15023,14);		-- Big number with 14-bit is 16383
					MyData1	<=		to_unsigned(14556,14);
					MyData2	<=		to_unsigned(13083,14);
					MyData3	<=		to_unsigned(13971,14);
					MyData4	<=		to_unsigned(14201,14);					
				end if;
				
			end if;
			
			if	(RDY = '1')	then
				Result		<=		Result(12 downto 0) & Dout;
			end if;
			
		end if;
		

--      wait;
   end process;

END;
