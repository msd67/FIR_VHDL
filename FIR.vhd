library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;

entity FIR is
--	Generic (																			--	Max Num with 12-bit = 4095
--		Mul_zero		:		INTEGER		:=	16383;
--		Mul_one		:		INTEGER		:=	1128;
--		Mul_two		:		INTEGER		:=	928;
--		Mul_three	:		INTEGER		:=	728;
--		Mul_four		:		INTEGER		:=	1328
--	);
	Port (
		clk		:		in				STD_LOGIC;
		Busy		:		out			STD_LOGIC;
		Valid		:		in				STD_LOGIC;
		RDY		:		out			STD_LOGIC;
		Din		:		in				STD_LOGIC;
		Dout		:		out			STD_LOGIC
	);
end FIR;

architecture Behavioral of FIR is

	signal	clock			:		STD_LOGIC	:=	'0';
	signal	inValid		:		STD_LOGIC	:=	'0';
	signal	outRDY		:		STD_LOGIC	:=	'0';
	
	signal	Action1		:		STD_LOGIC	:=	'0';
	signal	Action2		:		STD_LOGIC	:=	'0';
	signal	Busy_buff	:		STD_LOGIC	:=	'0';

	signal	Data_in		:		UNSIGNED	(13 downto 0)	:=	(OTHERS	=>	'0');	
	signal	Data_out		:		UNSIGNED	(13 downto 0)	:=	(OTHERS	=>	'0');
	signal	CPNT			:		UNSIGNED	(13 downto 0)	:=	(OTHERS	=>	'0');		--	Component
	signal	Result		:		UNSIGNED	(13 downto 0)	:=	(OTHERS	=>	'0');
	
	signal	B4				:		UNSIGNED	(25 downto 0)	:=	(OTHERS	=>	'0');		-- After Multiplication
	signal	B3				:		UNSIGNED	(25 downto 0)	:=	(OTHERS	=>	'0');
	signal	B2				:		UNSIGNED	(25 downto 0)	:=	(OTHERS	=>	'0');
	signal	B1				:		UNSIGNED	(25 downto 0)	:=	(OTHERS	=>	'0');
	signal	B0				:		UNSIGNED	(25 downto 0)	:=	(OTHERS	=>	'0');
	
	signal	Pause3		:		UNSIGNED	(25 DOWNTO 0)	:=	(OTHERS	=>	'0');		--	Delay Line
	signal	Pause2		:		UNSIGNED	(26 DOWNTO 0)	:=	(OTHERS	=>	'0');
	signal	Pause1		:		UNSIGNED	(27 DOWNTO 0)	:=	(OTHERS	=>	'0');
	signal	Pause0		:		UNSIGNED	(28 DOWNTO 0)	:=	(OTHERS	=>	'0');
	
	signal	Y				:		UNSIGNED	(28 DOWNTO 0)	:=	(OTHERS	=>	'0');
	
	signal	bit_count1	:		UNSIGNED	(3 downto 0)	:=	(OTHERS	=>	'0');		
	signal	bit_count2	:		UNSIGNED	(3 downto 0)	:=	(OTHERS	=>	'0');		
	signal	data_count	:		UNSIGNED	(2 downto 0)	:=	(OTHERS	=>	'0');
	signal	data_count2	:		UNSIGNED	(4 downto 0)	:=	(OTHERS	=>	'0');
	
	signal	Mul_zero		:		UNSIGNED	(11 downto 0)	:=	to_unsigned(832, 12);	--	0.203125 = 0.001101
	signal	Mul_one		:		UNSIGNED	(11 downto 0)	:=	to_unsigned(448, 12);	--	0.109375 = 0.000111
	signal	Mul_two		:		UNSIGNED	(11 downto 0)	:=	to_unsigned(1216, 12);	--	0.296875 = 0.010011
	signal	Mul_three	:		UNSIGNED	(11 downto 0)	:=	to_unsigned(128, 12);	--	0.03125  = 0.000010
	signal	Mul_four		:		UNSIGNED	(11 downto 0)	:=	to_unsigned(704, 12);	--	0.171875 = 0.001011
	
	component System_clock
	port
	 (-- Clock in ports
	  CLK_IN1           : in     std_logic;
	  -- Clock out ports
	  CLK_OUT1          : out    std_logic
	 );
	end component;

begin

	Main_Clock : System_clock
	  port map
		(-- Clock in ports
		 CLK_IN1 => CLK,
		 -- Clock out ports
		 CLK_OUT1 => clock);

	process (clock)
	begin
		if rising_edge(clock)	then
		
			Action1				<=		'0';			
			Action2				<=		'0';			
			Busy					<=		Busy_buff;
			inValid				<=		Valid;
			RDY					<=		outRDY;
			
			if	(inValid	=	'1')	then
				Data_in			<=		Data_in(12 downto 0) & Din;
				bit_count1		<=		bit_count1 + 1;
			end if;	
			
			if ( bit_count1	=	to_unsigned(12, 4))	then
				Busy_buff		<=		'1';
			end if;
			
			if (bit_count1	=	to_unsigned(13, 4))	then
				Busy_buff		<=		'0';	
			end if;
			
			if	( data_count = to_unsigned(6, 3) )	then						--	Five data have been given?
				data_count		<=		to_unsigned(1, 3);
				outRDY			<=		'1';
			end if;
			
			if ( data_count = to_unsigned(5, 3) )	then
				if	( bit_count1 = to_unsigned(0, 4)	)	then
					Action2			<=		'1';
				end if;
				
--				if ( bit_count1 = to_unsigned(8, 4) )	then
----					B0				<=		(OTHERS	=> '0');
----					B1				<=		(OTHERS	=> '0');
----					B2				<=		(OTHERS	=> '0');
----					B3				<=		(OTHERS	=> '0');
----					B4				<=		(OTHERS	=> '0');
--					
--					Pause0		<=		(OTHERS	=>	'0');
--					Pause1		<=		(OTHERS	=>	'0');
--					Pause2		<=		(OTHERS	=>	'0');
--					Pause3		<=		(OTHERS	=>	'0');
--				end if;
			end if;
			
			if	( outRDY = '1' )	then
				bit_count2		<=		bit_count2 + 1;
				Dout				<=		Data_out(13 - to_integer(bit_count2));
				if	( bit_count2 = to_unsigned(13, 4) )	then
					bit_count2	<=		(OTHERS	=>	'0');
					outRDY		<=		'0';
				end if;
			end if;
			
			if	( bit_count1	=	to_unsigned(14, 4))	then
			
				Action1			<=		'1';
				Action2			<=		'1';
				data_count		<=		data_count + 1;
				data_count2		<=		data_count2 + 1;
				bit_count1		<=		(OTHERS	=>	'0');
				CPNT				<=		Data_in;
				Data_in			<=		(OTHERS	=>	'0');
				
				Data_out			<=		Y(25 downto 12);						-- In 7 time Data_out will have correct value
			
			end if;			
			
			if	( Action1 = '1')	then
			
				B0					<=		CPNT	*	mul_zero;
				B1					<=		CPNT	*	mul_one;
				B2					<=		CPNT	*	mul_two;
				B3					<=		CPNT	*	mul_three;
				B4					<=		CPNT	*	mul_four;
				
			end if;
			
			if ( Action2 = '1' )	then
			
				Pause3			<=		B4;
				Pause2			<=		B3		+	Resize(Pause3, 27);
				Pause1			<=		B2		+	Resize(Pause2, 28);
				Pause0			<=		B1		+	Resize(Pause1, 29);			-- After 5 Calculation Pause0 have correct value
				Y					<=		B0		+	Pause0;							-- After 6 Calculation Y have correct value.	Go to Result
			end if;
			
		end if;
	end process;


end Behavioral;







